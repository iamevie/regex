use crate::token::Token;

#[derive(Debug)]
pub struct Lexer<'a> {
    source: &'a [u8],
    read: usize,
    current: u8,
}

impl<'a> Lexer<'a> {
    pub fn new(source: &'a str) -> Self {
        let source = source.as_bytes();
        let current = if !source.is_empty() { source[0] } else { b'\0' };

        Self {
            source,
            read: 0,
            current,
        }
    }

    pub fn next_token(&mut self) -> Token {
        let token = match self.current {
            b'?' => Token::Questionmark,
            b'+' => Token::Plus,
            b'*' => Token::Asteriks,
            b'.' => Token::Wildcard,
            b',' => Token::Comma,

            b'(' => Token::LParen,
            b')' => Token::RParen,
            b'{' => Token::LSquirly,
            b'}' => Token::RSquirly,
            b'[' => Token::LBracket,
            b']' => Token::RBracket,

            b'\\' => {
                self.advance(1);
                match self.current {
                    b't' => Token::Char('\t'),
                    b'f' => Token::Char(12 as char),
                    b'r' => Token::Char('\r'),
                    b'n' => Token::Char('\n'),
                    b's' => Token::Whitespace,
                    b'S' => Token::NonWhitespace,
                    b'w' => Token::Word,
                    b'W' => Token::NonWord,
                    b'b' => Token::WordBoundary,
                    b'B' => Token::NonWordBoundary,

                    _ => Token::Char(self.current as char),
                }
            }

            b'0'..=b'9' => {
                let start = self.read;
                while self.peek(1).is_ascii_digit() {
                    self.advance(1)
                }

                let ident = &self.source[start..=self.read];
                let digit = String::from_utf8_lossy(ident);
                match digit.parse::<usize>() {
                    Ok(digit) => Token::Digit(digit),
                    Err(e) => panic!("{e:?}"),
                }
            }

            b'\0' => Token::Eos,
            _ => Token::Char(self.current as char),
        };

        self.advance(1);
        token
    }

    #[inline(always)]
    fn advance(&mut self, by: usize) {
        self.current = self.peek(by);
        self.read += by;
    }

    #[inline(always)]
    fn peek(&self, by: usize) -> u8 {
        self[self.read + by]
    }

    #[inline(always)]
    fn len(&self) -> usize {
        self.source.len()
    }
}

impl std::ops::Index<usize> for Lexer<'_> {
    type Output = u8;

    #[inline(always)]
    fn index(&self, idx: usize) -> &Self::Output {
        if idx >= self.len() {
            &b'\0'
        } else {
            &self.source[idx]
        }
    }
}

impl Iterator for Lexer<'_> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        let token = self.next_token();
        match token {
            Token::Eos => None,
            _ => Some(token),
        }
    }
}
