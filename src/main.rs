use regex::parser::Parser;
use regex::print::print_pattern;

fn main() {
    let mut args = std::env::args();
    args.next();

    let regex = match args.next() {
        Some(regex) => regex,
        None => {
            eprintln!("Expected some RegEx");
            return;
        }
    };
    let mut parser = Parser::new(&regex);

    match parser.parse() {
        Ok(patterns) => patterns.into_iter().for_each(|p| {
            print_pattern(&p, 0);
        }),
        Err(err) => eprintln!("{err:?}"),
    }
}
