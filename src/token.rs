#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Token {
    Questionmark,
    Asteriks,
    Plus,
    Wildcard,
    Comma,

    LParen,
    RParen,
    LSquirly,
    RSquirly,
    LBracket,
    RBracket,

    NonWord,
    Word,
    NonWhitespace,
    Whitespace,
    NonWordBoundary,
    WordBoundary,

    Char(char),
    Digit(usize),

    Eos,
}
