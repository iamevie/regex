use crate::lexer::Lexer;
use crate::pattern::{Kind, Pattern, Quantity};
use crate::scope::Scope;
use crate::token::Token;

pub type ParserResult<T> = Result<T, ParserError>;
#[derive(Debug)]
pub enum ParserError {
    UnexpectedToken(char),
    InvalidTargetForQuantifier(Quantity),
    RangeValuesReversed(usize, usize),
    Unclosed(char),
    UnclosedScopeExpected(char),
}

#[derive(Debug, Clone, Copy)]
pub enum State {
	Normal,
	CharacterSet,
	Or,
}

pub struct Parser {
    source: Vec<Token>,
    read: usize,
    token: Token,
    scope: Box<Scope>,
	state: State,
}

impl<'a> Parser {
    pub fn new(source: &'a str) -> Self {
        let scope = Box::<Scope>::default();
        let source = Lexer::new(source).collect::<Vec<Token>>();
        let token = source[0];
		let state = State::Normal;
        Self {
            source,
            read: 0,
            token,
            scope,
			state,
        }
    }

    pub fn parse(&mut self) -> ParserResult<Box<Scope>> {
        while self.token != Token::Eos {
            let pattern = self.next_pattern()?;
            self.advance(1);
            self.scope.push(pattern);
        }

        Ok(self.scope.clone())
    }

    fn next_kind(&mut self) -> ParserResult<Kind> {
        Ok(match (self.state, self.token) {
			(State::CharacterSet, Token::Wildcard) => Kind::Char('.'),
			(State::CharacterSet, Token::Comma) => Kind::Char(','),
			(State::CharacterSet, Token::Questionmark) => Kind::Char('?'),
			(State::CharacterSet, Token::Asteriks) => Kind::Char('*'),
			(State::CharacterSet, Token::Plus) => Kind::Char('+'),
			(State::CharacterSet, Token::LParen) => Kind::Char('('),
			(State::CharacterSet, Token::LSquirly) => Kind::Char('{'),
			(State::CharacterSet, Token::LBracket) => Kind::Char('['),
			(State::CharacterSet, Token::RParen) => Kind::Char(')'),
			(State::CharacterSet, Token::RSquirly) => Kind::Char('}'),

            (_, Token::Questionmark | Token::Asteriks | Token::Plus) => {
                self.read -= 1;
                return Err(ParserError::InvalidTargetForQuantifier(self.quantity()?));
            }
            (_, Token::LParen) => {
				let mut state = State::CharacterSet;
				std::mem::swap(&mut state, &mut self.state);
				let mut scope = Scope::default();
				loop {
					self.advance(1);
					let pattern = match self.token {
						Token::Eos => return Err(ParserError::UnclosedScopeExpected(')')),
						Token::RParen => return Ok(Kind::CapturingGroup(Box::new(scope))),
						_ => self.next_pattern()?,
					};
					scope.push(pattern);
				}
			},

            (_, Token::RParen) => Kind::Char(')'),
            (_, Token::LBracket) => {
				let mut state = State::CharacterSet;
				std::mem::swap(&mut state, &mut self.state);
				let mut scope = Scope::default();

				loop {
					self.advance(1);
					let pattern = Pattern::new(
						match self.token {
							Token::Eos => return Err(ParserError::UnclosedScopeExpected(']')),
							Token::RBracket => {
								std::mem::swap(&mut state, &mut self.state);
								return Ok(Kind::CharacterSet(Box::new(scope)));
							}
							_ => self.next_kind()?
						},
						Quantity::Exactly(1)
					);
					scope.push(pattern);
				}	
			},
            (_, Token::RBracket) => Kind::Char(']'),
            (State::Normal, Token::LSquirly) => match self.peek(1) {
                Token::Digit(_) => match self.peek(2) {
                    Token::Comma => self.comma(3)?,
                    Token::RSquirly => {
                        self.read -= 1;
                        return Err(ParserError::InvalidTargetForQuantifier(self.quantity()?));
                    }
                    _ => Kind::Char('{'),
                },
                Token::Comma => self.comma(2)?,
                _ => Kind::Char('{'),
            },
			(_, Token::LSquirly) => Kind::Char('{'),

            (_, Token::Wildcard) => Kind::Wildcard,
            (_, Token::Whitespace) => Kind::Whitespace,
            (_, Token::NonWhitespace) => Kind::NonWhitespace,
            (_, Token::Word) => Kind::Word,
            (_, Token::NonWord) => Kind::NonWord,
            (_, Token::WordBoundary) => Kind::WordBoundary,
            (_, Token::NonWordBoundary) => Kind::NonWordBoundary,

            (_, Token::Comma) => Kind::Char(','),
            (_, Token::RSquirly) => Kind::Char('}'),
            (_, Token::Char(char)) => Kind::Char(char),
            (_, Token::Digit(digit)) => {
                let str = digit.to_string();
                self.source.remove(self.read);
                str.chars().enumerate().for_each(|(idx, char)| {
                    self.source.insert(self.read + idx, Token::Char(char));
                });
                self.token = self.peek(0);
                self.next_kind()?
            }

            (_, Token::Eos) => Kind::Eos,
        })
    }

    fn next_pattern(&mut self) -> ParserResult<Pattern> {
        Ok(Pattern::new(self.next_kind()?, self.quantity()?))
    }

    fn quantity(&mut self) -> ParserResult<Quantity> {
        let quantity = Ok(match (self.peek(1), self.peek(2), self.peek(3), self.peek(4), self.peek(5)) {
            (Token::Questionmark, _, _, _, _) => Quantity::Between(0..=1),
            (Token::Plus, _, _, _, _) => Quantity::ExactlyOrMore(1),
            (Token::Asteriks, _, _, _, _) => Quantity::ExactlyOrMore(0),
            (Token::LSquirly, Token::Comma, Token::Digit(digit), Token::RSquirly, _) => {
				self.advance(3);
				Quantity::Between(0..=digit)
            },
            (Token::LSquirly, Token::Digit(digit), Token::RSquirly, _, _) => {
				self.advance(2);
				Quantity::Exactly(digit)
            },
            (Token::LSquirly, Token::Digit(digit), Token::Comma, Token::RSquirly, _) => {
				self.advance(3);
				Quantity::ExactlyOrMore(digit)
            },
            (Token::LSquirly, Token::Digit(from), Token::Comma, Token::Digit(till), Token::RSquirly) => {
				self.advance(4);
				match from.cmp(&till) {
					std::cmp::Ordering::Greater => {
						return Err(ParserError::RangeValuesReversed(from, till))
					}
					std::cmp::Ordering::Equal => Quantity::Exactly(from),
					std::cmp::Ordering::Less => Quantity::Between(from..=till),
				}
			}
            _ => return Ok(Quantity::Exactly(1)),
        });

        self.advance(1);
        quantity
    }

    fn comma(&mut self, i: usize) -> ParserResult<Kind> {
        Ok(match (self.peek(i), self.peek(i + 1)) {
            (Token::Digit(_), Token::RSquirly) | (Token::RSquirly, _) => {
                self.read -= 1;
                return Err(ParserError::InvalidTargetForQuantifier(self.quantity()?));
            }
            _ => Kind::Char('{'),
        })
    }

    #[inline(always)]
    fn peek(&self, by: usize) -> Token {
        if (self.read + by) >= self.source.len() {
            Token::Eos
        } else {
            self.source[self.read + by]
        }
    }

    #[inline(always)]
    fn advance(&mut self, by: usize) {
        self.token = self.peek(by);
        self.read += by;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn run(regex: &str, patterns: &[Pattern]) {
        let mut parser = Parser::new(regex);
        let res = parser.parse().unwrap();

        assert_eq!(res.len(), patterns.len());

        res.into_iter()
            .enumerate()
            .for_each(|(i, p)| assert_eq!(p, patterns[i]));
    }

    #[test]
    fn chars() {
        run(
            "abcdef-90,",
            &[
                Pattern::new(Kind::Char('a'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('b'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('c'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('d'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('e'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('f'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('-'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('9'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('0'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char(','), Quantity::Exactly(1)),
            ],
        );

        for c in 33..=125 {
            let c = c as u8 as char;
            match c {
                '+' | '*' | '?' | '.' | '\\' | '(' | '[' | '|' | ')' => continue,
                _ => run(
                    &(c).to_string(),
                    &[Pattern::new(Kind::Char(c), Quantity::Exactly(1))],
                ),
            }
        }
    }

    #[test]
    fn quantity() {
        run(
            "a?(9*0){909}2+{2 }{2,}p{1,9}u",
            &[
                Pattern::new(Kind::Char('a'), Quantity::Between(0..=1)),
                Pattern::new(
                    Kind::CapturingGroup(Box::new(Scope {
                        patterns: vec![
                            Pattern::new(Kind::Char('9'), Quantity::ExactlyOrMore(0)),
                            Pattern::new(Kind::Char('0'), Quantity::Exactly(1)),
                        ],
                    })),
                    Quantity::Exactly(909),
                ),
                Pattern::new(Kind::Char('2'), Quantity::ExactlyOrMore(1)),
                Pattern::new(Kind::Char('{'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('2'), Quantity::Exactly(1)),
                Pattern::new(Kind::Char(' '), Quantity::Exactly(1)),
                Pattern::new(Kind::Char('}'), Quantity::ExactlyOrMore(2)),
                Pattern::new(Kind::Char('p'), Quantity::Between(1..=9)),
                Pattern::new(Kind::Char('u'), Quantity::Exactly(1)),
            ],
        )
    }

    #[test]
    fn character_set() {
        run(
            "[a-bcd{2}]",
            &[Pattern::new(
                Kind::CharacterSet(Box::new(Scope {
                    patterns: vec![
                        Pattern::new(Kind::Range(b'a'..=b'b'), Quantity::Exactly(1)),
                        Pattern::new(Kind::Char('c'), Quantity::Exactly(1)),
                        Pattern::new(Kind::Char('d'), Quantity::Exactly(1)),
                        Pattern::new(Kind::Char('{'), Quantity::Exactly(1)),
                        Pattern::new(Kind::Char('2'), Quantity::Exactly(1)),
                        Pattern::new(Kind::Char('}'), Quantity::Exactly(1)),
                    ],
                })),
                Quantity::Exactly(1),
            )],
        )
    }
}
