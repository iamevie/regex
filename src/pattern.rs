use crate::scope::Scope;
use std::ops::RangeInclusive;

#[derive(Debug, Clone, PartialEq)]
pub struct Pattern {
    pub kind: Kind,
    pub quantity: Quantity,
}

impl Pattern {
    pub fn new(kind: Kind, quantity: Quantity) -> Self {
        Self { kind, quantity }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Kind {
    Char(char),
    Not(char),
    Wildcard,
    Whitespace,
    NonWhitespace,
    Word,
    NonWord,
    WordBoundary,
    NonWordBoundary,

    LineStart,
    LineEnd,

    Or(Vec<Scope>),

    Range(RangeInclusive<u8>),
    CharacterSet(Box<Scope>),
    CapturingGroup(Box<Scope>),

    Eos,
    Digit(usize),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Quantity {
    Exactly(usize),
    Between(RangeInclusive<usize>),
    ExactlyOrMore(usize),
}
