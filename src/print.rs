use crate::pattern::{Kind, Pattern, Quantity};
use crate::scope::Scope;

fn tabs(mut depth: usize) {
    if depth == 0 {
        return;
    }
    depth *= 4;
    print!("{:>depth$}", ' ')
}

pub fn print_pattern(pattern: &Pattern, depth: usize) {
    tabs(depth);
    match &pattern.kind {
        Kind::Char(char) => print!("Char: {char:?}\t"),
        Kind::Digit(digit) => print!("Digit: {digit:?}\t"),
        Kind::Not(char) => print!("Not: {char:?}\t"),
        Kind::Whitespace => print!("Whitespace: \\s\t"),
        Kind::NonWhitespace => print!("NonWhitespace: \\S"),
        Kind::Word => print!("Word: \\w"),
        Kind::NonWord => print!("NonWord: \\W"),
        Kind::WordBoundary => print!("WordBoundary: \\b"),
        Kind::NonWordBoundary => print!("NonWordBoundary: \\B"),
        Kind::Wildcard => print!("Wildcard\t"),
        Kind::Or(possibilities) => {
            print!("Or\t");
            for p in possibilities {
                print_scope(p, &pattern.quantity, "", depth + 1);
            }
        }
        Kind::Range(range) => print!("Range: {range:?}"),
        Kind::CharacterSet(scope) => {
            print_scope(scope, &pattern.quantity, "CharacterSet\t", depth);
            return;
        }
        Kind::CapturingGroup(scope) => {
            print_scope(scope, &pattern.quantity, "CapturingGroup", depth);
            return;
        }
        _ => todo!(),
    }
    println!("\tquantity: {:?}", pattern.quantity)
}

fn print_scope(scope: &Scope, quantity: &Quantity, tag: &str, depth: usize) {
    tabs(depth);
    println!("{tag}\tquantity: {quantity:?}");
    scope
        .patterns
        .iter()
        .for_each(|p| print_pattern(p, depth + 1))
}
