use crate::pattern::Pattern;
use std::ops::{Index, IndexMut};

#[derive(Debug, Clone, Default, PartialEq)]
pub struct Scope {
    pub patterns: Vec<Pattern>,
}

impl Scope {
    pub fn push(&mut self, pattern: Pattern) {
        self.patterns.push(pattern)
    }

    pub fn len(&self) -> usize {
        self.patterns.len()
    }
}

impl Index<usize> for Scope {
    type Output = Pattern;

    fn index(&self, idx: usize) -> &Self::Output {
        &self.patterns[idx]
    }
}

impl IndexMut<usize> for Scope {
    fn index_mut(&mut self, idx: usize) -> &mut Self::Output {
        &mut self.patterns[idx]
    }
}

impl IntoIterator for Scope {
    type Item = Pattern;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.patterns.into_iter()
    }
}
